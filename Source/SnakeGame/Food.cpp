// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "Math/UnrealMathUtility.h" 
#include "Borders.h"
#include "Components/SphereComponent.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{    
	MoveFood();
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
		}
	}
}

void AFood::MoveFood()
{
	FVector sphere1 = SphereComponent1->GetComponentLocation();
	FVector sphere2 = SphereComponent2->GetComponentLocation();

	
	
	
	FVector NewLocation;
	

	float RandX = FMath::Rand() / (float)FMath::Rand();
	float RandY = FMath::Rand() / (float)FMath::Rand();

	NewLocation.X = RandX * 150.f;
	NewLocation.Y = RandY * 150.f;

	SetActorLocationAndRotation(NewLocation, FRotator(0, 0, 0));
	
}


