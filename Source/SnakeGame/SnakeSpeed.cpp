// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeSpeed.h"
#include "SnakeBase.h"
#include "Math/UnrealMathUtility.h"  
#include "Borders.h"
#include "Components/SphereComponent.h"

// Sets default values
ASnakeSpeed::ASnakeSpeed()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASnakeSpeed::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASnakeSpeed::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnakeSpeed::Interact(AActor* Interactor, bool bIsHead)
{
	
	
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			//float newSpeed = Snake->GetSpeed()*1.05f;
			//Snake->SetSpeed(newSpeed);
			Snake->SetSpeed(Snake->GetSpeed() * 1.05f);
		}
		
	}
		MoveFoodSpeed();
}

void ASnakeSpeed::MoveFoodSpeed()
{
	FVector NewLocation;


	float RandX = FMath::Rand() / (float)FMath::Rand();
	float RandY = FMath::Rand() / (float)FMath::Rand();

	NewLocation.X = RandX * 150.f;
	NewLocation.Y = RandY * 150.f;

	SetActorLocationAndRotation(NewLocation, FRotator(0, 0, 0));
	
}

