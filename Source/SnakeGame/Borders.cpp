// Fill out your copyright notice in the Description page of Project Settings.


#include "Borders.h"
#include "Components/SphereComponent.h"
#include "Math/Vector.h"
#include "Engine/BlendableInterface.h"
#include "Math/UnrealMathUtility.h" 
//#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
ABorders::ABorders()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    //MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
}

// Called when the game starts or when spawned
void ABorders::BeginPlay()
{
	Super::BeginPlay();
    
    
    //float x = FMath::Rand(sphere1.X, sphere2.X);
   //float y = FMath::Rand(sphere1.Y, sphere2.Y);
    //float distance = RandomFloat(sphere1.Size(), sphere2.Size());

    
    SphereComponent1 = NewObject<USphereComponent>(this, USphereComponent::StaticClass());
    SphereComponent1->RegisterComponent();
    SphereComponent1->AttachToComponent(GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform);
    SphereComponent1->InitSphereRadius(30.0f);
    SphereComponent1->SetCollisionProfileName(TEXT("Pawn"));
    SphereComponent1->SetRelativeLocation(FVector::ZeroVector);
    SphereComponent1->SetVisibility(true);
    
    SphereComponent2 = NewObject<USphereComponent>(this, USphereComponent::StaticClass());
    SphereComponent2->RegisterComponent();
    SphereComponent2->AttachToComponent(GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform);
    SphereComponent2->InitSphereRadius(30.0f);
    SphereComponent2->SetCollisionProfileName(TEXT("Pawn"));
    SphereComponent2->SetRelativeLocation(FVector::ZeroVector);
    SphereComponent2->SetVisibility(true);
}

// Called every frame
void ABorders::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABorders::Sphere()
{
	
}

